from urllib import request
from django.shortcuts import render
from django.views import View
# Create your views here.

class Principal(View):
    def get(self,request):

        datos={
            'titulo': 'Control de Basura',
            'encabezado': 'Gestion y control de basuras UD FJC',
            'buscar': 'buscar zona',
            'zonas': 'Zonas de recoleccion',
            'solicitar':'Solicitar monitoreo',
            'contacto': 'Contacto',
            'sistema':'Sistema de control de basuras',
            'informacion':'¿Quienes somos?',
            'convenios':'Convenios'
        }

        return render(request,'principal/TPrincipal.html',datos)
